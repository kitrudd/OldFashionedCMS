<?php

require '../conf/config.php';
include 'loginFunctions.php';
include 'editorFunctions.php';
include 'contentFunctions.php';

# Database connection
function sqlConnect() {
	$connect = new mysqli(sqlhost(),sqluser(),sqlpass(),sqldb());
	return $connect;
}

# Search function
function search($query) {
	$user = $_SESSION['userPriv'] ?? 'public';
	if (strlen($query) < 1) {
		echo "Please enter a search query.";
	} else {
		$stripped = strip_tags(trim($query)); //trims whitespace and strips HTML
		$tags = array("<",">"); 
		$trimmed = str_replace($tags,"",$stripped); 

		# Check for an empty string and display a message.

		if ($trimmed == "") {
			$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries where entryid='0'";
			echo "No search terms entered.";
		} elseif (strlen($trimmed) < 3) {
			$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries where entryid='0'";
			echo "Please enter three characters or more";
		} else { 
			if ($user == "admin") {
				$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries where entry like '%$trimmed%' ORDER BY entryid DESC";
			} else {
				$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries where entry like '%$trimmed%' AND status != '0' ORDER BY entryid DESC";
				$print = "entry";
			}
		} 
		$resultID = mysqli_query(sqlConnect(),$retrieve) or die("");
		return $resultID;
	}
}

# Inserts a blank line after the printed string
function println($str) {
	print ("$str<br>\n");
}

# An error handling function that will print the error message then exit the script
function errorHandling($message) {
   echo '<b>An error has occurred:</b><br><br><i>';
   echo $message;
}

# Retrieves page entries and creates navigation menu from results
function pageNav() {
	$user = $_SESSION['userPriv'] ?? 'public';
	if ($user == "admin") {
		$retrieve = "SELECT pageid,title FROM pages";
	} else {
		$retrieve = "SELECT pageid,title FROM pages WHERE visible ='1'";
	}
	$resultID = mysqli_query(sqlConnect(),$retrieve) or die("");
	if ((mysqli_num_rows($resultID) > 0)) {
		while ($row=mysqli_fetch_row($resultID)) {
			print "<li class='li".$row[0]."'><a href='page.php?t=p&e=".$row[0]."'>".$row[1]."</a></li>";	
		}
	} else {
		print ("No nav records");
	}
}

?>