SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `OldFashionedDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `OldFashionedDB`;

CREATE TABLE `entries` (
  `entryid` int(11) NOT NULL,
  `category` int(2) DEFAULT NULL,
  `title` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `entry` longtext COLLATE latin1_general_ci,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `status` tinyint(1) NOT NULL,
  `blurb` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `errors` (
  `errorid` int(11) NOT NULL,
  `title` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `entry` longtext COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `pages` (
  `pageid` int(11) NOT NULL,
  `title` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `entry` longtext COLLATE latin1_general_ci,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `visible` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `uname` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT INTO `users` (`user_id`, `uname`, `pazz`) VALUES
(1, 'admin', 'your_password_hash');

ALTER TABLE `entries`
  ADD PRIMARY KEY (`entryid`),
  ADD KEY `date` (`date`);

ALTER TABLE `errors`
  ADD UNIQUE KEY `errorid` (`errorid`);

ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageid`),
  ADD UNIQUE KEY `entryid` (`pageid`),
  ADD KEY `date` (`date`),
  ADD KEY `entryid_2` (`pageid`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `uname` (`uname`);


ALTER TABLE `entries`
  MODIFY `entryid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pages`
  MODIFY `pageid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
