<?php
	# Start the session and import all functions
	session_start();
	require 'functions/functions.php';
	
	# Check for privileged user
	userPriv();
?>

<!doctype html>
<html>

	<?php
		# Site Header: HTML declaration and head
		require 'templates/layout/siteHeader.php';
	?>
	
	<body id="<?= $bodyID ?>">
	
		<?php
			# Branding including site header
			include 'templates/layout/header.php'; 
		?>
		
		<div id="mainContainer">
		
			<?php
				# Navigation, Search Box, and Admin (if logged in) templates
				include 'templates/layout/navigation.php';
				include 'templates/panels/search.php';
				include 'templates/admin/admin.php';
			?>

			<div id="fullContent">

				<?php
					# Specify entry type and number for query
					$type = $_REQUEST['t'] ?? "e";
					$entry = $_REQUEST['e'] ?? 0;
					
					if ($type == "s") {
						$result = search($entry);
					} else {
						$result = selectEntries($type,$entry);
					}
					
					# Print Results
					printEntries($result,$type);
				?>
			</div>

		</div>

		<?php
			# Footer: Copyright notice and RSS link
			include 'templates/layout/footer.php';	
		?>
	
	</body>
</html>