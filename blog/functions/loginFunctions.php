<?php

# Checks a supplied password against the password hash contained in the database
function loginHash( $uname, $password ) {
	$hashed = hash( 'sha256', $password, false );
	$uname = mysqli_escape_string(sqlConnect(),$uname);
	$query = "SELECT uname,pass FROM users WHERE uname = '".$uname."'";
	$result = mysqli_query(sqlConnect(),$query) or die(mysqli_error(sqlConnect())); 

	if (mysqli_num_rows($result) > 0) {
		while ($row=mysqli_fetch_row($result)) {
			if( $row[1] == $hashed ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

# Creates a sha256 hash from a supplied password and commits the new credentials to the database
function setHash( $uname, $password ) {
	$uname = mysqli_escape_string(sqlConnect(),$uname );
	$newpass = mysqli_escape_string(sqlConnect(),$password ); 
	$newpass = hash( 'sha256', $password, false );
	$result = mysqli_query(sqlConnect(),"SELECT uname,pass FROM users WHERE uname ='" . $uname . "'") or die(); 
	$set = "UPDATE users SET pass = '$newpass' WHERE uname = '$uname'";
	mysqli_query(sqlConnect(),$set) or die("Couldn't Update hash: ".mysqli_error(sqlConnect()));
}

# Resets all session and request variables, then checks the supplied credentials against the admin entry in the database.
# If both username and password hash match, a new session variable, "userPriv" is set to "admin"
function userPriv() {
	$status = $_REQUEST['status'] ?? "";

	# Privilage reset
	if ($status == "logout") {
		unset($_SESSION['user']);
		unset($_SESSION['userPriv']);
		unset($_REQUEST['user']);
		unset($_REQUEST['pass']);
		session_destroy();
		return true;
	}

# Credential check
	if ((isset($_REQUEST["user"])) && (isset($_REQUEST["pass"]))) {
		if (loginHash($_REQUEST['user'],$_REQUEST['pass'])) {
			$_SESSION["userPriv"] = "admin";
		} else {
			unset($_SESSION['userPriv']);
		}
	}
	return true;
}

# User check
function userExists($fieldValue, $tableName, $fieldName) {
	$key = mysqli_query(sqlConnect(),"SELECT username,name,password,admin FROM $tableName WHERE $fieldName = '$fieldValue'") or die(mysqli_error(sqlConnect()));
	return(mysqli_num_rows($key) == 1);
}

?>