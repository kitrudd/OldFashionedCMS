<?php

# Create a new blog entry - 1st 800 characters are set aside for the short blurb on the front page
function newEntry($category,$title,$entry,$dateStamp,$pubStatus) {
	$shortBlurb = substr($entry,0,799);
	$shortBlurb = addslashes($shortBlurb);
	$shortBlurb = str_replace("&","&amp;",$shortBlurb);
	
	$title = addslashes($title);
	$title = str_replace("&","&amp;",$title);
	
	$entry = addslashes($entry);
	$entry = str_replace("&","&amp;",$entry);
	
	$insertEntry = "INSERT INTO entries (category,title,entry,date,status,blurb) VALUES ('$category','$title','$entry','$dateStamp','$pubStatus','$shortBlurb')";
	
	mysqli_query(sqlConnect(),$insertEntry) or die(mysqli_error(sqlConnect()));
}

# Create a new page - is added to navigation menu on create
function newPage($title,$entry,$dateStamp,$pubStatus) {
	$title = addslashes($title);
	$title = str_replace("&","&amp;",$title);
	
	$entry = addslashes($entry);
	$entry = str_replace("&","&amp;",$entry);
	
	$insertPage = "INSERT INTO pages (title,entry,date,visible) VALUES ('$title','$entry','$dateStamp','$pubStatus')";
	
	mysqli_query(sqlConnect(),$insertPage) or die("bawls.");
}

# Edit an existing blog entry
function editEntry($idToEdit,$category,$title,$entry,$pubStatus) {
	$shortBlurb = substr($entry,0,799);
	$shortBlurb = str_replace("&","&amp;",$shortBlurb);
	
	$entry = str_replace("&","&amp;",$entry);
	
	$title = addslashes($title);
	
	$dateStamp = date('Y-m-d');
	
	$update = "UPDATE entries SET title = '$title',category = '$category',entry = '$entry',date = '$dateStamp',status = '$pubStatus',blurb = '$shortBlurb' WHERE entryid = '$idToEdit'";
	
	mysqli_query(sqlConnect(),$update) or die("Couldn't Update Record: ".mysqli_error(sqlConnect()));	
}

# Edit an existing page
function editPage($idToEdit,$title,$entry,$visible) {
	$entry = str_replace("&","&amp;",$entry);
	
	$title = addslashes($title);
	
	$update = "UPDATE pages SET title = '$title',entry = '$entry',visible = '$visible' WHERE pageid = '$idToEdit'";
	
	mysqli_query(sqlConnect(),$update) or die("Couldn't Update Record: ".mysqli_error(sqlConnect()));
}

# Delete an existing entry
function deleteEntry($idToDelete) {
	$deleteRecord = "DELETE FROM entries WHERE entryid='$idToDelete'";
	mysqli_query(sqlConnect(),$deleteRecord) or die("Error: Can't delete record:".mysqli_error(sqlConnect()));
}

# Delete an existing page
function deletePage($toDelete) {
	$deleteRecord = "DELETE FROM pages WHERE pageid='$toDelete'";
	mysqli_query(sqlConnect(),$deleteRecord) or die("Error: Can't delete record:".mysqli_error(sqlConnect()));
}

# Changes variables to edit based on type (entry|page)
function mapEditVars($type, $queryResult) {
	switch ($type) {
		case "e":
			while ($row=mysqli_fetch_row($queryResult)) {
				$category = stripslashes($row[1]);
				$title = stripslashes($row[2]);
				$body = stripslashes($row[3]);
				$status = stripslashes($row[5]);
			}
		break;
		case "p":
			while ($row=mysqli_fetch_row($queryResult)) {
				$title = stripslashes($row[1]);
				$body = stripslashes($row[2]);
				$status = stripslashes($row[4]);
				$category = "";
			}
		break;
		default:
	}
	$valueArray = array($title,$body,$status,$category);
	return $valueArray;
}
?>