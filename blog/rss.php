<?php
require 'functions/functions.php';
userPriv();

header("Content-Type: application/rss+xml");

echo ("<?xml version='1.0' encoding='UTF-8' ?><rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>");
?>

<channel>
<atom:link href="https://www.<?= $siteUrl ?>/rss.php" rel="self" type="application/rss+xml" />
<title><?= $siteTitle ?> Articles</title>
<link>https://www.<?= $siteUrl ?>/</link>
<description><?= $feedDescription ?></description>

<?php


# Connect to the MySQL server: Error handling

$entryRetrieve = "SELECT entryid,category,title,entry,date,status,blurb FROM entries WHERE status ='1' ORDER BY entryid DESC";
$rssConnect = sqlConnect();
$resultID = mysqli_query($rssConnect,$entryRetrieve) or die("");

# Prints out entries returned from query

if (mysqli_num_rows($resultID) == 0) {
	echo "No entries!";
} else {
	if ((mysqli_num_rows($resultID) > 0)) {
		while ($row=mysqli_fetch_row($resultID)) {
			$description = formatRSS($row[2]);
			$pubDate = date("D, d M Y H:i:s O", strtotime($row[4]));
			?> 
			<item><title><?php echo $row[1]; ?></title>
			<guid isPermaLink="true">https://www.<?= $siteUrl ?>/page.php?e=<?php echo $row[0]; ?></guid>
			<description><?php echo $description; ?></description>
			<pubDate><?php echo $pubDate; ?></pubDate>
			</item> 
			<?php
		}
	}
}

mysqli_close($rssConnect);
echo ("</channel></rss>");
?>