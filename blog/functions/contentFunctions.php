<?php 

# Select entries based on type and id
function selectEntries($type, $id) {
	$user = $_SESSION['userPriv'] ?? 'public';
	switch ($type) {
		case "e": # entry
			if ($user == "admin") {
				$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries WHERE entryid = '$id'";
			} else {
				$retrieve = "SELECT entryid,category,title,entry,date FROM entries WHERE status != '0' AND entryid = '$id'";
			}
			break;
		
		case "p": # page
			if ($user == "admin") {
				$retrieve = "SELECT pageid,title,entry,date,visible FROM pages WHERE pageid = $id";
			} else {
				$retrieve = "SELECT pageid,title,entry,date,visible FROM pages WHERE visible ='1' AND pageid = $id";
			}
			break;
		
		case "pr": # projects
			$retrieve = "SELECT entryid,category,title,entry,date FROM entries WHERE status != '0' AND category='5' ORDER BY entryid DESC";
			break;
	
		case "all": # all entries
			if($user == "admin") { # if you are admin...grab an entry regardless of lock
				$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries ORDER BY entryid DESC";
			} else {
				$retrieve = "SELECT entryid,category,title,entry,date,status FROM entries WHERE status !='0' ORDER BY entryid DESC";
			}
			break;
		default:
	} 
	$resultID = mysqli_query(sqlConnect(),$retrieve);
	return $resultID;
}

# Print selected entries
function printEntries($selectResult,$printType) {
	$user = $_SESSION['userPriv'] ?? 'public';

	if (mysqli_num_rows($selectResult) < 1) {
		echo "No entries returned.";
	} else {
		if ((mysqli_num_rows($selectResult) > 0)) {
			while ($row=mysqli_fetch_row($selectResult)) {
				$sqlString = strtotime(stripslashes($row[4]));
				$dateString = date('M d Y', $sqlString);
					
				print "<article class='hentry'><header><h1 class='entry-title'>";
				
				$sqlPubAttrib = $row[5] ?? 0;
				
				if (($sqlPubAttrib == 0) || ($sqlPubAttrib == 1)) {
					$pubAttrib = "published";
				} else if ($sqlPubAttrib == 2) {
					$pubAttrib = "updated";
					$dateString = "Updated: ".$dateString;
				} else {
				}
				
				if (($printType == "e" ) || ($printType == "pr")) {
					print(stripslashes($row[2])."</h1><time datetime=".stripslashes($row[4])." pubdate class=".$pubAttrib.">".$dateString."</time></header>".stripslashes($row[3]));
				} else if ($printType == "p") {
					print (stripslashes($row[1])."</h1></header>".stripslashes($row[2])); 
				} else {}
				
				?>
				<footer>
				<?php                
				
				# Edit and Delete Buttons                
				if( $user == "admin") {
				?>
					<div class="editButtons"><a href="edit.php?t=<?php echo $printType; ?>&	e=<?php echo $row[0]; ?>">Edit Entry</a></div><!--end of editButton-->
				<?php
				}
				?>
				</footer>
				</article><!--end article-->
				<?php
			}
		}
	} 
}

# Changes carets to HTML codes for RSS feed
function formatRSS($toFormat) {
	$formatted = str_replace("<","&lt;",$toFormat);
	$formatted = str_replace(">","&gt;",$formatted);
	return ($formatted);
}
?>