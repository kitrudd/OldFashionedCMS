<?php
	# Start the session and import all functions
	session_start();
	require 'functions/functions.php';
	
	# Check for privileged user
	userPriv();
?>

<!doctype html>
<html>

	<?php
		# Site Header: HTML declaration and head
		require 'templates/layout/siteHeader.php';
	?>
	
	<body id="<?= $bodyID ?>">
	
		<?php
			# Branding including site header
			include 'templates/layout/header.php'; 
		?>
		
		<div id="mainContainer">
		
			<?php
				# Navigation, Search Box, and Admin (if logged in) templates
				include 'templates/layout/navigation.php';
				include 'templates/panels/search.php';
				include 'templates/admin/admin.php';
			?>
		
			<div id="fullContent">
				<?php
					# Display the form, if user is admin
					$userPriv = $_SESSION['userPriv'] ?? "";
					$submitted = $_REQUEST['submitted'] ?? "";

					if ($userPriv == "admin") {
						if ($submitted != "Update") {
							$action = "";
							$category = "";
							$title = "";
							$body = "";
							$status = "";
		
							if ( (isset($_REQUEST['t'])) && (isset($_REQUEST['e'])) ) {
								$toEdit = $_REQUEST['e'] ?? 0;
								$type = $_REQUEST['t'] ?? "e";
								
								$editResult = selectEntries($type,$toEdit);
								$resultArray = mapEditVars($type,$editResult);

							}
							else if ( (isset($_REQUEST['t'])) && (!(isset($_REQUEST['e']))) ) {
								$type = $_REQUEST['t'];
								$resultArray = array("Title","Body","Status","6");
							} else {
								# Create a dummy array. The category is outside the bounds which makes DB-side cleanup easier
								$resultArray = array("Title","Body","Status","10");
							}
						?>
		
						<?php
							/* EDIT FORM FOLLOWS */
						?>
						<form action="edit.php" method="post">
							<ul>
								<?php 
									if ($type == "e") {
								?>
								<li>Category: (Currently <?php echo $resultArray[3] ?>)
									<SELECT NAME="category" tabindex='1'>
										<OPTION value="<?php echo $resultArray[3] ?>">Choose</OPTION>
										<OPTION value="1">1: General</OPTION>
										<OPTION value="2">2: Rants</OPTION>
										<OPTION value="3">3: CompSci</OPTION>
										<OPTION value="4">4: Story</OPTION>
										<OPTION value="5">5: Project</OPTION>
									</SELECT>
								</li>
								<?php
									}
								?>
								<li>Title: <TEXTAREA name="title" rows="1" cols="20" tabindex='2'><?php echo $resultArray[0] ?></TEXTAREA></li>
								<li>Entry:</br><TEXTAREA name="body" rows="20" cols="60" tabindex='3'><?php echo $resultArray[1]; ?></TEXTAREA></li>
								<li><input type="hidden" name="ed" value="<?php echo $toEdit; ?>"></li>
								<li><SELECT NAME="pubStatus" value="<?php echo $resultArray[2]; ?>" tabindex='5'>
									<?php if ($type == "e") { ?>
										<OPTION value="0">Draft</OPTION>
										<OPTION value="1">Published</OPTION>
										<?php if (isset($_REQUEST['e'])) { ?>
											<OPTION value="2">Updated</OPTION>
										<?php }
									} else {
										?>
										<OPTION value="0">Hidden</OPTION>
										<OPTION value="1">Visible</OPTION>
										<?php
									}
									?>
									</SELECT>
								</li>
								<li><?php 	if (!(isset($_REQUEST['e']))) { ?>
												<INPUT type="hidden" name="op" value="new_<?php echo $type; ?>">
												<?php
											} else { ?>
												<SELECT NAME="op" tabindex='6'>
												<OPTION value="ed_<?php echo $type; ?>">Edit</OPTION>
												<OPTION value="del_<?php echo $type; ?>">Delete</OPTION>
												<?php
											}
									?>
									</SELECT>
								</li>
								<li>
									<input type="submit" name="submitted" value="Update">
								</li>
							</ul>
						</form>
		
						<?php
		
						/* End of Unsubmitted. If the form is submitted, the following occurs */
		
						} elseif ($_REQUEST['submitted'] == "Update") {
							
							# Gather and sanitize request variables  
							$toEdit = $_REQUEST["ed"];
							$title = $_REQUEST["title"];
							$body = $_REQUEST["body"];
							$body = addslashes($body);
							$category=$_REQUEST["category"] ?? 1;
							$status=$_REQUEST["pubStatus"] ?? 0;
							$operation = $_REQUEST["op"];
		
							# Create and format the date entry in RFC-822 Format for RSS feed
							$dateStamp = date('Y-m-d');
					
							# Set blank error check string
							$errs =  "";
				
							# Verify editable fields. All others contain default values
									
							if (strlen($title) < 1) {
								$errs .= "<li>Please enter a title.</li>";
							}
							
							if (strlen($body) < 1) {
								$errs .= "<li>This entry is blank.</li>";
							}
					
							# Add additional tests here
							if (strlen($errs) > 0) {
								echo("Please correct the following errors:<ul>$errs</ul>");
							} 
							else {
								# No errors present. Continue processing...
								switch ($operation) {
									case "new_e":
										newEntry($category,$title,$body,$dateStamp,$status);
									break;
									
									case "ed_e":
										editEntry($toEdit,$category,$title,$body,$status);
										unset($_REQUEST['op']);
										unset($_REQUEST['ed']);
									break;
									
									case "del_e":
										deleteEntry($toEdit);
									break;
									
									case "new_p":
										newPage($title,$body,$dateStamp,$status);
									break;
									
									case "ed_p":
										editPage($toEdit,$title,$body,$status);
										unset($_REQUEST['op']);
										unset($_REQUEST['ed']);
									break;
									
									case "del_p":
										deletePage($toEdit);
									break;
									
									default:{ }
								} 
					
								println("<html><head><meta http-equiv='refresh' content='2;URL=index.php'></head><B><center>Operation Successful</center></B>");                   
							}
						} else {
							echo "That is not a valid value. Please go back and try again."; 
						}
					} else {
						# User is not admin 
						echo "You aren't signed in. Please sign in via the sign-in box or on the main page."; 
					}
				?>
			</div>

		</div>

		<?php
			# Footer: Copyright notice and RSS link
			include 'templates/layout/footer.php';	
		?>
		
	</body>
</html>