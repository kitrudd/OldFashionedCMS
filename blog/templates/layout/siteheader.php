<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<META NAME="ROBOTS" CONTENT="NOARCHIVE" />
	<link rel="alternate" type="application/rss+xml" title="RSS" href="rss.php" />

	<link rel="stylesheet" href="styles/default.css">

	<!--[if IE]>
		<link rel="stylesheet" href="styles/iedefault.css" />
	<![endif]-->

	<title><?= $siteTitle ?></title>
</head>