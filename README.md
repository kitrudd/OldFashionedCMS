## Name
OldFashionedCMS - An Apache/MySQL/PHP CMS project. Started in 2005, retired in 2018, and resurrected in 2023.

## Description
OldFashionedCMS utilizes a basic web-based admin interface for managing menu pages and blog entries, accessed via the login.php page. The admin user is created and added to the DB before attempting to log in.

## Installation
OldFashionedCMS has been tested most recently on MAMP version 6.8 with the following versions:
Apache
MySQL v5.7.39
PHP v8.2.0

Web server settings from phpMyAdmin:
Apache/2.4.54 (Unix) OpenSSL/1.0.2u PHP/8.2.0 mod_wsgi/3.5 Python/2.7.18 mod_fastcgi/mod_fastcgi-SNAP-0910052141 mod_perl/2.0.11 Perl/v5.30.1

IMPORTANT: Place the conf folder one level ABOVE your web server htdocs or wwwroot folder. The config.php file is where you define your MySQL DB connection credentials and should be properly secured via permissions and placement outside the web server document root.

## Usage
1. Install your W/M/L + AMP stack of choice
	
2. Rename conf/config.php.sample to config.php
	
3. Fill out the following variables in config.php:
	1. $siteTitle - This displays in the title bar, footer, and RSS feed
	2. $siteUrl - This is the url of the site, not including the "www"
	3. $bodyID - A unique ID for the body tag for css targeting purposes
	4. $feedDescription - RSS feed site description
	5. $copyrightYear - Goes in footer next to $siteTitle
	6. $host - MySQL DB host
	7. $sqluser - MySQL DB User
	8. $sqlpass - MySQL DB Password
	9. $db - MySQL Database, default is OldFashionedDB

4. Save config.php

5. Upload to your webhost of choice

6. Edit line 44 in sample_db.sql and replace your_password_hash with a sha256 hash of your password. An easy way to create one on macos 13.4.1 Ventura is: echo -n your_password | shasum -a 256

7. Optional: Change "OldFashionedDB" on lines 10-11 to whatever you want, just make sure to change your $db variable in config.php to match the new name.

8. Import MySQL DB file conf/sample_db.sql to create the MySQL DB. Delete conf/sample_db.sql after import.

9. Start your web server and navigate to login.php to log in and start creating entries

## A Valid Old Fashioned Recipe
~~~
<?xml version="1.0" encoding="UTF-8"?>
<OldFashioned>
	<Glass>
		<SimpleSyrup />
		<Bitters />
		<Bourbon />
		<Ice />
		<Garnish>
			<Cherry>
				<Type>Luxardo</Type>
				<Muddle>False</Muddle>
			</Cherry>
			<OrangeSlice>
				<Muddle>False</Muddle>
			</OrangeSlice>
		</Garnish>
	</Glass>
</OldFashioned>
~~~